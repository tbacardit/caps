#!/usr/bin/python
import glob, os, struct, imghdr, sys, shutil
lastname = "Non-Defined";
def rename(dir, pattern):
	for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
		title, ext = os.path.splitext(os.path.basename(pathAndFilename))
		width, height=get_image_size(os.path.basename(pathAndFilename))
		if width == 180 and height == 180 or width == 87 and height == 87 or width == 66 and height == 66:
			sizePattern=r'%s@3x'
		elif width == 120 and height == 120 or width == 152 and height == 152 or width == 58 and height == 58 or width == 1536 and height == 2048 or width == 2048 and height == 1536 or width == 80 and height == 80 or width == 44 and height == 44:
			sizePattern=r'%s@2x'
		elif width == 76 and height == 76 or width == 768 and height == 1024 or width == 1024 and height == 768 or width == 60 and height == 60 or width == 29 and height == 29 or width == 22 and height == 22:
			sizePattern=r'%s@1x'
		else:
			sizePattern=r'%s@2x'
		print(sizePattern)
		var = ""
		var = raw_input("enter name of file "+title+" Height: "+str(height)+" Width: "+str(width) +" Marked as " + sizePattern+". Leave empty to use same as last time: "+lastname+" ")
		if var == "":
			global lastname
			var = lastname
			print("in")
		else:
			global lastname
			lastname = var
		os.rename(pathAndFilename,
			os.path.join(dir, sizePattern % var + ext))
		if width == 1024 and height == 1024:
			repeat = raw_input("this file has to be also done in @1x and @3x as it is the app icon for the app store and IT IS REQUIERED. Press 1 to continue and 0 to abort ")
			if repeat == 1:
				copy_rename(sizePattern % var + ext, r'%s@1x' % var + ext)
				copy_rename(sizePattern % var + ext, r'%s@3x' % var + ext)
			
def copy_rename(old_file_name, new_file_name):
	src_dir= os.curdir
	if not os.path.exists("subfolder"):
		os.mkdir("subfolder")
	dst_dir= os.path.join(os.curdir , "subfolder")
	src_file = os.path.join(src_dir, old_file_name)
	shutil.copy(src_file,dst_dir)
	
	dst_file = os.path.join(dst_dir, old_file_name)
	new_dst_file_name = os.path.join(dst_dir, new_file_name)
	os.rename(dst_file, new_dst_file_name)
	shutil.copy(dst_file, src_dir)
	shutil.copy(new_dst_file_name,src_dir)

def rename_1x(dir, pattern):
	for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
		title, ext = os.path.splitext(os.path.basename(pathAndFilename))
		sizePattern=r'%s@1x'
		os.rename(pathAndFilename,
				  os.path.join(dir, sizePattern % title + ext))

def rename_2x(dir, pattern):
	for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
		title, ext = os.path.splitext(os.path.basename(pathAndFilename))
		sizePattern=r'%s@2x'
		os.rename(pathAndFilename,
				  os.path.join(dir, sizePattern % title + ext))


def rename_3x(dir, pattern):
	for pathAndFilename in glob.iglob(os.path.join(dir, pattern)):
		title, ext = os.path.splitext(os.path.basename(pathAndFilename))
		sizePattern=r'%s@3x'
		os.rename(pathAndFilename,
				  os.path.join(dir, sizePattern % title + ext))


def get_image_size(fname):
	with open(fname, 'rb') as fhandle:
		head = fhandle.read(24)
		if len(head) != 24:
			return
		if imghdr.what(fname) == 'png':
			check = struct.unpack('>i', head[4:8])[0]
			if check != 0x0d0a1a0a:
				return
			width, height = struct.unpack('>ii', head[16:24])
		elif imghdr.what(fname) == 'gif':
			width, height = struct.unpack('<HH', head[6:10])
		elif imghdr.what(fname) == 'jpeg':
			try:
				fhandle.seek(0) # Read 0xff next
				size = 2
				ftype = 0
				while not 0xc0 <= ftype <= 0xcf:
					fhandle.seek(size, 1)
					byte = fhandle.read(1)
					while ord(byte) == 0xff:
						byte = fhandle.read(1)
					ftype = ord(byte)
					size = struct.unpack('>H', fhandle.read(2))[0] - 2
				# We are at a SOFn block
				fhandle.seek(1, 1)  # Skip `precision' byte.
				height, width = struct.unpack('>HH', fhandle.read(4))
			except Exception: #IGNORE:W0703
				return
		else:
			return
		return width, height

##it's from the directory it's placed in .
menu=True
while (menu):
	print("Remember: This only renames .png files, nor it will rename tab icons in manual mode (as they have non-fixed sizes and overlap with other sizes)")
	print("Select operation or write which Iphone or Ipad version you want to rename for.")
	print("1.Automode")
	print("2.@1x (iPad2 and iPad mini)")
	print("3.@2x (iPhone 6s, iPhone 6, iPhone 5, iPad, iPad mini, iPhone 4 and iPad pro)")
	print("4.@3x (iPhone 6s Plus and iPhone 6 Plus)")
	print("5.Exit")

	choice = raw_input("Enter choice(1/2/3/4/5):")
	if choice == '1':
		rename(os.getcwd(), r'*.png')
	elif choice == '2':
		rename_1x(os.getcwd(), r'*.png')
	elif choice == '3':
		rename_2x(os.getcwd(), r'*.png')
	elif choice == '4':
		rename_3x(os.getcwd(), r'*.png')
	elif choice == '5':
		menu=False
		print ("Goodbye!")
		break;
	else:
		print("Invalid input")
    

