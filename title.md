# Welcome to Complementary Apple Python Scripts.

This is a project to help small nitpicks I personally have with xcode (small things, such as batch-renaming of .png assets)

All of the .py count as having python installed at /usr/bin/python do "python where" if you're unsure if that's correct for you.

How to add a script as an alias, in console:
````
cd
nano .bash_profile
alias NAME='python /SCRIPTFOLDER/SCRIPTNAME'
````